class AddIndexToUsersEmployeeId < ActiveRecord::Migration
  def change
  	add_index :users, :employee_id, unique: true
  end
end
