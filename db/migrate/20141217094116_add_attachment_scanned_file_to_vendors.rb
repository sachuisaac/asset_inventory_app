class AddAttachmentScannedFileToVendors < ActiveRecord::Migration
  def self.up
    change_table :orders do |t|
      t.attachment :scanned_file
    end
  end

  def self.down
    remove_attachment :orders, :scanned_file
  end
end
