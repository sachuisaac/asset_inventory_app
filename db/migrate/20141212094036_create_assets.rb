class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :location
      t.string :name
      t.string :description
      t.string :asset_type
      t.string :assigned_to
      t.string :status
      t.string :model_no
      t.string :serial_no
      t.string :mac_address
      t.string :purchase_date
      t.string :cost, default: '0'
      t.boolean :amc
      t.string :amc_amount, default: '0'
      t.string :expiry_date
      t.string :status_date
      t.string :warranty_type
      # t.binary :image_url
      t.string :IS_type
      t.string :integrity
      t.string :Confidentiality
      t.string :note

      t.references :user, index: true
      t.references :vendor, index: true
      t.references :owner, index: true
      t.references :order, index: true

      t.timestamps
    end
  end
end
