class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :scanned_file
      
      t.timestamps
    end
  end
end
