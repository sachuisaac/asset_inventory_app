class AddDeactiveColumnToAssets < ActiveRecord::Migration
  def change
  	add_column :assets, :deactivate, :boolean
  end
end
