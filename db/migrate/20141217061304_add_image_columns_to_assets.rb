class AddImageColumnsToAssets < ActiveRecord::Migration
  def change
  	add_attachment :assets, :image_url
  end
end
