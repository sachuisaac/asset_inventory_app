# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141229130601) do

  create_table "assets", force: true do |t|
    t.string   "location"
    t.string   "name"
    t.string   "description"
    t.string   "asset_type"
    t.string   "assigned_to"
    t.string   "status"
    t.string   "model_no"
    t.string   "serial_no"
    t.string   "mac_address"
    t.string   "purchase_date"
    t.string   "cost",                   default: "0"
    t.boolean  "amc"
    t.string   "amc_amount",             default: "0"
    t.string   "expiry_date"
    t.string   "status_date"
    t.string   "warranty_type"
    t.string   "IS_type"
    t.string   "integrity"
    t.string   "Confidentiality"
    t.string   "note"
    t.integer  "user_id"
    t.integer  "vendor_id"
    t.integer  "owner_id"
    t.integer  "order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deactivate"
    t.string   "image_url_file_name"
    t.string   "image_url_content_type"
    t.integer  "image_url_file_size"
    t.datetime "image_url_updated_at"
  end

  add_index "assets", ["order_id"], name: "index_assets_on_order_id"
  add_index "assets", ["owner_id"], name: "index_assets_on_owner_id"
  add_index "assets", ["user_id"], name: "index_assets_on_user_id"
  add_index "assets", ["vendor_id"], name: "index_assets_on_vendor_id"

  create_table "orders", force: true do |t|
    t.string   "name"
    t.string   "scanned_file"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "scanned_file_file_name"
    t.string   "scanned_file_content_type"
    t.integer  "scanned_file_file_size"
    t.datetime "scanned_file_updated_at"
    t.integer  "user_id"
  end

  create_table "owners", force: true do |t|
    t.string   "client"
    t.string   "process"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "employee_id"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.boolean  "admin",           default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["employee_id"], name: "index_users_on_employee_id", unique: true

  create_table "vendors", force: true do |t|
    t.string   "name"
    t.string   "manager"
    t.string   "address"
    t.string   "phone_no"
    t.string   "phone_no_other"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "versions", force: true do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
    t.string   "ip"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"

end
