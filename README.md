Asset Inventory Management

This application will be used by users (facility administrators) to add, edit, list and deactive (not delete) assets. They will also use the application to track the assets by their status. The status of the assets will be changed during their life cycle and so a history has to be maintained for each asset.

Functions:

a) Register users
b) Login
c) Add new assets
d) A paginated table to list assets
e) View asset details
f) Edit asset
g) Search assets by name, type, owner, status and purchase date
h) Deactivate asset
i) Export asset to csv


