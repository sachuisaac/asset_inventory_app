class AssetDatatable < AjaxDatatablesRails::Base
  # uncomment the appropriate paginator module,
  # depending on gems available in your project.
  # include AjaxDatatablesRails::Extensions::Kaminari
  include AjaxDatatablesRails::Extensions::WillPaginate
  # include AjaxDatatablesRails::Extensions::SimplePaginator

  def_delegators :@view, :link_to, :edit_asset_path, :deactivate_path, :image_tag, :history_path

  def sortable_columns
    # list columns inside the Array in string dot notation.
    # Example: 'users.email'
    @sortable_columns ||= [
        'assets.name',
        'assets.location',
        'assets.description',
        'assets.asset_type',
        'assets.owner_id',
        'assets.assigned_to',
        'assets.status',
        'assets.model_no',
        'assets.serial_no',
        'assets.mac_address',
        'assets.purchase_date',
        'assets.order_id',
        'assets.vendor_id',
        'assets.cost',
        'assets.amc',
        'assets.amc_amount',
        'assets.expiry_date',
        'assets.status_date',
        'assets.warranty_type',
        'assets.IS_type',
        'assets.integrity',
        'assets.Confidentiality',
        'assets.note'
    ]
  end

  def searchable_columns
    # list columns inside the Array in string dot notation.
    # Example: 'users.email'
    @searchable_columns ||= [
        'assets.name',
        'assets.location',
        'assets.description',
        'assets.asset_type',
        'assets.owner_id',
        'assets.assigned_to',
        'assets.status',
        'assets.model_no',
        'assets.serial_no',
        'assets.mac_address',
        'assets.purchase_date',
        'assets.order_id',
        'assets.vendor_id',
        'assets.cost',
        'assets.amc',
        'assets.amc_amount',
        'assets.expiry_date',
        'assets.status_date',
        'assets.warranty_type',
        'assets.IS_type',
        'assets.integrity',
        'assets.Confidentiality'
    ]
  end

  private

  def data
    records.map do |record|
      [
        # "<a href=\"/assets/#{record.id}/edit\">#{record.name}</a>",
        record.name,
        record.location,
        record.description,
        record.asset_type,
        !(record.owner.nil?) ? record.owner.name : "",
        !record.user.nil? ? record.user.name : "",
        record.status,
        record.model_no,
        record.serial_no,
        record.mac_address,
        !record.order.nil? ? link_to("#{record.order.name}", record.order.scanned_file.url) : "",
        # record.order.scanned_file.url,
        record.purchase_date,
        !record.vendor.nil? ? record.vendor.name : "",
        record.cost,
        record.amc,
        record.amc_amount,
        record.expiry_date,
        record.status_date,
        record.warranty_type,
        !record.image_url.nil? ? link_to(image_tag(record.image_url.url(:thumb)), record.image_url.url) : "",
        record.IS_type,
        record.integrity,
        record.Confidentiality,
        record.note,
        link_to("Edit", edit_asset_path(record)),
        link_to("Deactivate", deactivate_path(record)),
        link_to("History", history_path(record), remote: true, 'data-toggle' => "modal", 'data-target' => "#historyModal")
        # <button class="btn btn-default" data-toggle="modal" data-target="#ownerModal">Create Owner</button>
        # "<a href=\"/assets/#{record.id}/deactivate\">Deactivate</a>"
      ]
    end
  end

  def get_raw_records
    User.find_by(id: params[:id]).assets.where(deactivate: false)
    # User.find_by(id: params[:id]).assets.find_by(deactivate: false)
  end

  # ==== Insert 'presenter'-like methods below if necessary
end