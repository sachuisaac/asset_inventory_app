class Order < ActiveRecord::Base
  has_many :assets
  belongs_to :user

  # has_paper_trial
  
  has_attached_file :scanned_file
  do_not_validate_attachment_file_type :scanned_file
end
