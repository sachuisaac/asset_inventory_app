class Vendor < ActiveRecord::Base
  has_many :assets

  # has_paper_trial

  validates :phone_no, numericality: { only_integer: true }, format: { with: /\A\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*\z/}
end
