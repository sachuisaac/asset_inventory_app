class Asset < ActiveRecord::Base
  belongs_to :user
  belongs_to :owner
  belongs_to :vendor
  belongs_to :order

  has_paper_trail

  accepts_nested_attributes_for :order

  STATUS = %w[active spare faulty returned servicing inactive]
  TYPE = %w[Desktop Laptop monitor mouse keyboard headset mobile tablet printer router firewall switch software]
  WARRANTY_TYPE = %w[1-year 2-years 3-years 4-years 5-years perpetual]
  HML = %w[High Medium Low]
  
  before_save { self.mac_address = mac_address.upcase}

  after_initialize :init

  has_attached_file :image_url,
  					        :styles => { :medium => "300x300>", :thumb => "50x50>", :small => "25x25>" }

  validates :user_id, presence: true
  validates :name, presence: true, length: {maximum: 100}
  validates :location, length: {maximum: 100}
  validates :asset_type, presence: true
  validates :status, presence: true
  validates :mac_address, allow_blank: true, format: { with: /\A([0-9A-F]{2}[:-]){5}([0-9A-F]{2})\z/, message: "Invalid MAC address" }
  validates :cost, numericality: { only_integer: true}
  validates :IS_type, length: { maximum: 50}
  validates_attachment_content_type :image_url, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def init
  	self.deactivate = false if self.deactivate.nil?
  end
end
