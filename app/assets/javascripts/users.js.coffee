table_ready = ->
  'use strict'
  # $(".datetimepicker").datetimepicker pickTime: false
  format = (d) ->
    return "<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\" style=\"padding-left:50px;\">" + "<tr>" + "<td>Full name:</td>" + "<td>" + "" + "</td>" + "</tr>" + "<tr>" + "<td>Extension number:</td>" + "<td>" + "d.extn" + "</td>" + "</tr>" + "<tr>" + "<td>Extra info:</td>" + "<td>And any further details here (images etc)...</td>" + "</tr>" + "</table>"
  oTable = $('#assets-table').DataTable
    scrollX: true
    info: false
    # stateSave: true
    processing: true
    responsive: 
      details:
        renderer: (api, rowIdx) ->
          data1 = api.cells(rowIdx, ":hidden").eq(0).map((cell) ->
            header = $(api.column(cell.column).header())
            "<td><strong>" + header.text() + ":" + "</strong></td>"
          ).toArray().join("")
          data2 = api.cells(rowIdx, ":hidden").eq(0).map((cell) ->
            header = $(api.column(cell.column).header())
            "<td>" + api.cell(cell).data() + "</td>"
          ).toArray().join("")
          data = "<tr>" + data1 + "</tr><tr>" + data2 + "</tr>"
          (if data then $("<table/>").append(data) else false)
    serverSide: true
    ajax: $('#assets-table').data('source')
    columns: [
      {
        "className": 'details-control',
        "orderable": false,
        "searchable": false,
        "data": null,
        "defaultContent": ''
      },
      {"data": "0" },
      {"data": "1" },
      {"data": "2" },
      {"data": "3" },
      {"data": "4" },
      {"data": "5" },
      {"data": "6" },
      {"data": "7" },
      {"data": "8" },
      {"data": "9" },
      {"data": "10"},
      {"data": "11" },
      {"data": "12" },
      {"data": "13" },
      {"data": "14" },
      {"data": "15" },
      {"data": "16" },
      {"data": "17" },
      {"data": "18" },
      {"data": "19" },
      {"data": "20" },
      {"data": "21" },
      {"data": "22" },
      {"data": "23" },
      {"data": "24" },
      {"data": "25" },
      {"data": "26" }
    ]
    pagingType: 'full_numbers'
    language: {
      "sSearch": "Filter Assets"
    }
    sDom: "<'row-fluid'<'span6'l><'span6'f>r>Tt<'row-fluid'<'span6'i><'span6'p>>",
    tableTools:  {
        sSwfPath: "http://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"
        "aButtons": [
            "copy",
            "print",
            {
                "sExtends":    "collection",
                "sButtonText": 'Save <span class="caret" />',
                "aButtons":    [ "csv", "xls" ]
            }
         ]
    }
  yadcf.init oTable, [
    { column_number: 0, data: gon.assetList, filter_container_id: "assetName", filter_default_label: "All Assets"},
    { column_number: 3, data: gon.typeList, filter_container_id: "assetType", filter_default_label: "All Types"},
    { column_number: 4, data: gon.ownerList, filter_container_id: "assetOwner", filter_default_label: "All Owners"},
    { column_number: 6, data: gon.statusList, filter_container_id: "assetStatus", filter_default_label: "All Status"},
    { column_number: 5, data: gon.assignedToList, filter_container_id: "assetAssignedTo", filter_default_label: "All Employees"},
    { column_number: 12, data: gon.vendorList, filter_container_id: "assetVendor", filter_default_label: "All Vendors"},
    { column_number: 1, filter_container_id: "assetLocation", filter_type: "auto_complete", filter_default_label: "Location", text_data_delimiter: ","},
    { column_number: 7, filter_container_id: "assetModel", filter_type: "auto_complete", filter_default_label: "Model Number", text_data_delimiter: ","}
   ]
  
  # $("#assets-table tbody").on "click", "td.details-control", ->
  #   tr = $(this).closest("tr")
  #   row = oTable.row(tr)
  #   if row.child.isShown()
  #     row.child.hide()
  #     tr.removeClass "parent"
  #   else
  #     row.child(format()).show()
  #     tr.addClass "parent"
  #   return

$(document).ready table_ready

$(document).on "page:load", table_ready







# responsive: 
#       details:
#         renderer: (api, rowIdx) ->
#           data = api.cells(rowIdx, ":hidden").eq(0).map((cell) ->
#             header = $(api.column(cell.column).header())
#             "<tr>" + "<td>" + header.text() + ":" + "</td> " + "<td>" + api.cell(cell).data() + "</td>" + "</tr>"
#           ).toArray().join("")
#           (if data then $("<table/>").append(data) else false)