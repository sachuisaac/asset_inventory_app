class OwnersController < ApplicationController
	before_action :logged_in_user, only: [:create, :new]

	def create
		@owner = Owner.new(owner_params)
		if @owner.save
			flash[:success] = "New Owner Created!!"
			redirect_to current_user
		end
	end

	private
		def owner_params
			params.require(:owner).permit(:client, :process, :name)
		end
end
