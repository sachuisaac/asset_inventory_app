class AssetsController < ApplicationController
	before_action :logged_in_user, only: [:create, :new]
	before_action :current_asset, only: [:edit, :update]

	def new
		@asset = Asset.new
		@order = Order.new
		@orderList = current_user.orders.all
		gon.watch.orderList = @orderList
	end

	def create
		@asset = current_user.assets.build(asset_params)
		if @asset.save
			flash.now[:success] = "Asset created"
			redirect_to current_user
		else
			render 'new'
		end
	end

	def edit
		@asset = Asset.find(params[:id])
	end

	def update
      @asset = Asset.find(params[:id])
      if @asset.update_attributes(asset_params)
        flash.now[:success] = "Asset Updated"
        redirect_to current_user
      else
        render 'edit'
      end
    end

    def deactivate
    	@asset = Asset.find(params[:id])
    	if @asset.update_attributes(deactivate: true)
    		flash.now[:success] = "Asset Deactivated"
    		redirect_to current_user
    	else
    		flash.now[:error] = "Asset Deactivation failed"
    		redirect_to current_user
    	end
    end

    def history
    	@versions = Asset.find(params[:id]).versions
    	respond_to do |format|
    		format.html
		    format.js
		end
	end



	private
		def asset_params
			params.require(:asset).permit(:location, :name, :description, :asset_type, :owner_id, :assigned_to, :status, :model_no, :serial_no, :mac_address, :purchase_date, :order_id, :vendor_id, :cost, :amc, :amc_amount, :expiry_date, :status_date, :warranty_type, :image_url, :IS_type, :integrity, :Confidentiality, :note, orders_attributes: [:name, :scanned_file])
		end

		def current_asset
	      @asset = current_user.assets.find_by(id: params[:id])
	      redirect_to current_user if @asset.nil?
	    end

end
