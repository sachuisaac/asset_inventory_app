class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :set_cache_buster

  def set_cache_buster
   response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
   response.headers["Pragma"] = "no-cache"
   response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  protect_from_forgery with: :exception
  include SessionsHelper

  def info_for_paper_trail
    { ip: request.remote_ip }
  end

  def user_for_paper_trail
    logged_in? ? current_user.id : 'Guest'
  end

  private
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end


end
