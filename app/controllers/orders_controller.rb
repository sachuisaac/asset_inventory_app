class OrdersController < ApplicationController
	before_action :logged_in_user, only: [:create]

	def create
		@order = current_user.orders.build(order_params)
		if @order.save
			flash.now[:success] = "New Order Created!!"
		else
			flash.now[:danger] = "Order not created"
		end
	end

	private
		def order_params
			params.require(:order).permit(:name, :scanned_file)
		end

end
