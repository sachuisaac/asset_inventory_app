class VendorsController < ApplicationController
	before_action :logged_in_user, only: [:create, :new]

	def create
		@vendor = Vendor.new(vendor_params)
		if @vendor.save
			flash[:success] = "New Vendor Created!!"
			redirect_to current_user
		else
			flash[:danger] = "Vendor not created"
			redirect_to current_user 
		end
	end

	private
		def vendor_params
			params.require(:vendor).permit(:name, :manager, :address, :phone_no, :phone_no_other)
		end

end
