class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :show]
  before_action :correct_user, only: [:edit, :update, :show]

  def new
  	@user = User.new
  end

  def create
  	@user = User.new(admin_params)
  	if @user.save
      if @user.admin == true
        log_in @user
        flash[:success] = "Welcome to the AMS!"
  		  redirect_to @user
      else
        flash[:info] ="Employee Portal is under construction!! Visit us sometime later!"
        redirect_to root_path
      end
  	else
  		render 'new'
  	end
  end

  def show
    @vendor = Vendor.new
    @owner = Owner.new

    initialiseGONvariables
    
  	@user = User.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: AssetDatatable.new(view_context) }
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile Updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private
  	def user_params
  		params.require(:user).permit(:name, :employee_id, :email, :password, :password_confirmation)
  	end

    def admin_params
      params.require(:user).permit(:name, :employee_id, :email, :password, :password_confirmation, :admin)
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in"
        redirect_to login_url
      end
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to( current_user ) unless @user == current_user
    end

    def initialiseGONvariables
      assets = User.find_by(id: params[:id]).assets.where(deactivate: false)
      gon.assetList = Set.new
      gon.typeList = Set.new
      gon.ownerList = Set.new
      gon.statusList = Set.new
      gon.assignedToList = Set.new 
      gon.vendorList = Set.new
      assets.each do |asset|
        gon.assetList << asset.name
        gon.typeList << asset.asset_type
        if !asset.owner.nil?
          gon.ownerList << { value: asset.owner.id, label: asset.owner.name}
        end
        gon.statusList << asset.status
        if !asset.user.nil?
          gon.assignedToList << { value: asset.user.id, label: asset.user.name}
        end
        if !asset.vendor.nil?
          gon.vendorList << { value: asset.vendor.id, label: asset.vendor.name}
        end
      end
    end
end
