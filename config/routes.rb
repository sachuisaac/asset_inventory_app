Rails.application.routes.draw do
 
  root 'static_pages#home'
  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  resources :users
  resources :assets
  resources :owners, only: [:create]
  resources :vendors, only: [:create]
  resources :orders, only: [:create]

  get '/assets/:id/deactivate', to: 'assets#deactivate', as: :deactivate
  get '/assets/:id/history', to: 'assets#history', as: :history
  
end
